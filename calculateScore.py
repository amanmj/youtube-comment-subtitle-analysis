from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet

treebankToWordnet = {
    'N': 'n',  # Noun
    'V': 'v',  # Verb
    'J': 'a',  # Adjective
    'R': 'r'   # Adverb
}


def getSimilarityScore(comment, subtitle):

    # Tokenize the 2 sentences
    tokenizedComment = word_tokenize(comment)
    tokenizedSubtitle = word_tokenize(subtitle)

    # Classify into parts of speech
    taggedComment = pos_tag(tokenizedComment)
    taggedSubtitle = pos_tag(tokenizedSubtitle)


    # Get the first best synset for the tagged words

    commentSynset = []

    for i in taggedComment:
        firstsynset = bestSynset(i[0],i[1])
        if firstsynset is not None:
            commentSynset.append(firstsynset)

    subtitleSynset = []

    for i in taggedSubtitle:
        firstsynset = bestSynset(i[0],i[1])
        if firstsynset is not None:
            subtitleSynset.append(firstsynset)

    score = 0.0
    count = 0

    # For each word in the comment
    for i in commentSynset:

        # Get the similarity value of the most similar word in the subtitle
        best_score = 0
        for j in subtitleSynset:
            best_score = max(best_score, i.path_similarity(j))

        # if a best score is found
        if best_score > 0:
            score += best_score
            count += 1

    # Return Average
    if count != 0:
        score /= count
    return score


def treeBankToWordnetTag(tag):
    # Convert Treebank tag to Wordnet tag

    if tag is None or tag[0] is None:
        return None
    try:
        return treebankToWordnet[tag[0]]
    except:
        return None


def bestSynset(word, tag):
    wordNetTag = treeBankToWordnetTag(tag)
    if wordNetTag is None:
        return None
    try:
        tagList = wordnet.synsets(word, wordNetTag)
        if tagList is None or tagList[0] is None:
            return None
        return tagList[0]
    except:
        return None


def calculateAverage(comments, subtitles, filename):
    finalScores = []
    for i in comments:
        count = 0
        total_score = 0.0
        for j in subtitles:
            score = getSimilarityScore(i, j)
            if score > 0:
                count += 1
                total_score += score
        avg_score = 0.0
        if count > 0:
            avg_score = total_score/count
        finalScores.append((i,avg_score))
        print 'Avg score for \"' + i + '\" = ' + str(avg_score)

    finalScores = sorted(finalScores, key=lambda x:x[1], reverse = True)
    writeResultTofile(finalScores, filename)


def writeResultTofile(finalScores, fileName):
    resultFile = open('results/' + fileName, 'w')
    resultFile.write('**************Top 10 Results for ' + fileName + '***************\n')

    resultSet = set()

    for i in finalScores:
        comment = i[0].lower()
        resultSet.add(comment)
        if len(resultSet) == 10:
            break

    cnt = 0
    for i in resultSet:
        cnt += 1
        resultFile.write(str(cnt) + '. ' + i + '\n')

    resultFile.close()


if __name__ == '__main__':
    print getSimilarityScore('i am hungry','i am starving')
