# List of Youtube Videos:

1. http://youtube.com/watch?v=tiMY7-h3xnE
2. http://youtube.com/watch?v=XJGiS83eQLk
3. http://youtube.com/watch?v=Ncjcg97P1Tg
4. http://youtube.com/watch?v=KcWXKmnZZVo
5. http://youtube.com/watch?v=SSo_EIwHSd4

# File locations

1. The results are present in the `results` folder.
2. The subtitles are present in the `subtitles` folder.
3. The comments for each video are present in the `comments` folder.
4. The similarity score calculation logic is in `calculateScore.py`
5. The fetching of youtube comments via data API and reading file logic is in `youtube.py`

# Algorithm to get top 10 comments

1. For each comment iterate over each subtitle and calculate score
2. Average the summed score if score > 0
3. Save it in a set
4. Sort the set in descending order and pick the top 10 comments from the set
5. Save it in a file

# Algorithm to calculate score between comment and subtitle

1. tokenize the subtitle and comment
2. Classify the 2 tokenized sentences into parts of speech using pos_tag()
3. Convert treebank tag to first wordnet tag from the tagged subtitle and comment
4. For each element in the comment synset, iterate over each element in subtitle synset and calculate the best score for that word
5. If the score is positive, total it up for every element in subtitle synset
6. Repeat this for each word in comment synset
7. Calculate the average for the above summed scores

# How to run this

1. There are 5 separate hard coded function calls for each video in youtube.py
2. Uncomment the `saveComments` function and the corresponding `readFile`
3. The `saveComments` will gather all the comments using youtube v3 data API and save it in a file
4. The `readFile` will read the subtitle and comment file and calculate the score
5. The subtitles have been generated using youtube-dl