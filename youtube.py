import requests
import simplejson
from calculateScore import calculateAverage

API_URL = 'https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&maxResults=100&key=AIzaSyDypcqlHHaaDMf4aSOZqOPssQuzGsMW-qk&videoId='


def saveComments(filename, videoId):

    url = API_URL + videoId
    req = requests.get(url)
    content = req.content
    jsonContent = simplejson.loads(content)

    f = open('comments/'+filename, 'w')

    commentsJson = jsonContent['items']

    flag = True

    while flag is True:
        for i in commentsJson:
            comment = i['snippet']['topLevelComment']['snippet']['textOriginal']
            print comment
            comment = comment.encode('ascii', 'ignore')
            f.write(comment.rstrip() + '\n')

        if 'nextPageToken' not in jsonContent:
            break

        nextPageToken = jsonContent['nextPageToken']
        tempurl = url + '&pageToken=' + nextPageToken
        req = requests.get(tempurl)
        content = req.content
        jsonContent = simplejson.loads(content)
        newCommentsJson = jsonContent['items']
        if newCommentsJson == commentsJson:
            break
        commentsJson = newCommentsJson

    f.close()


def readFile(comment, subtitle):
    commentFile = open('comments/'+comment, 'r')
    content = commentFile.readlines()
    comments = []
    for i in content:
        comments.append(i.rstrip())
    commentFile.close()

    subtitleFile = open('subtitles/'+subtitle, 'r')
    content = subtitleFile.readlines()
    subtitles = []
    for i in content:
        subtitles.append(i.rstrip())
    subtitleFile.close()

    calculateAverage(comments, subtitles, comment)


if __name__ == '__main__':
    #saveComments('video_1', 'tiMY7-h3xnE')
    #readFile('video_1', 'video_1.vtt')

    #saveComments('video_2', 'XJGiS83eQLk')
    #readFile('video_2', 'video_2.vtt')

    #saveComments('video_3', 'Ncjcg97P1Tg')
    #readFile('video_3', 'video_3.vtt')

    #saveComments('video_4', 'KcWXKmnZZVo')
    #readFile('video_4', 'video_4.vtt')

    #saveComments('video_5', 'SSo_EIwHSd4')
    #readFile('video_5', 'video_5.vtt')
